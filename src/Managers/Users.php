<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\User;
use Msnet\Amocrm\Collections;

class Users extends Manager
{
    public function getLocal()
    {
        $response = $this->sendRequest(
            'GET', 'account', ['with' => 'users']
        );
        
        if ($response->isSuccess())
        {
            $result = [];
            
            foreach ($response->getData()['_embedded']['users'] as $id => $user)
            {
                $result[] = new User($user, $id);
            }
        }

        return $result ? new Collections\Users($result) : false;
    }
}