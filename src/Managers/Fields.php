<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Field;
use Msnet\Amocrm\Collections;

class Fields extends Manager
{
    /**
     * @param int $id ID поля
     */
    public function get(int $id = null)
    {
        $response = $this->sendRequest(
            'GET', 'account', ['with' => 'custom_fields']
        );
        
        if ($response->isSuccess())
        {
            $result = [];
            
            foreach ($response->getData()['_embedded']['custom_fields'] as $type => $items)
            {
                foreach ($items as $id => $item)
                    $result[$type][] = new Field($item, $id);
            }
        }

        return $result ? new Collections\Fields($result) : false;
    }

    /**
     * @param array $fields
     */
    public function add(array $fields)
    {
        $data = [
            'add' => $this->getObjectsData($fields, Field::class)->removeFields(['id'])
        ];

        $data['add']->forEach(function($i, &$item) {
            $item->enums = $item->enums->map(function($i, $item) {
                return $item->value;
            });
        });

        $data['add']->rename('field_type', 'type');
        $data['add'] = $data['add']->removeFields(['sort', 'is_multiple', 'is_system', 'is_editable', 'is_required', 'is_visible', 'params']);

        return $this->sendRequest('POST', 'fields/', $data);
    }

    /**
     * @param array $fields
     */
    public function delete(array $fields)
    {
        $data = [
            'add' => $this->getObjectsData($fields, Field::class)->selectFields(['id'])
        ];

        return $this->sendRequest('POST', 'fields/', $data);
    }
}