<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Account;
use Msnet\Amocrm\Collections;

class Accounts extends Manager
{
    /**
     * @param array $fields Доплнительные поля для получения
     */
    public function getCurrent(array $fields = [])
    {
        $response = $this->sendRequest(
            'GET', 'account', ['with' => join(',', $fields)]
        );
        
        if ($response->isSuccess())
            return new Account($response->getData()['_embedded']);
            
        return false;
    }
}