<?php

namespace Msnet\Amocrm\Managers;

use Msnet\Amocrm\Base\Manager;
use Msnet\Amocrm\Contact;
use Msnet\Amocrm\Collections;

class Contacts extends Manager
{
    /**
     * @param array $contacts Массив объектов типа Contact
     */
    public function add(array $contacts)
    {
        $data = [
            'add' => $this->getObjectsData($contacts, Contact::class)
        ];

        return $this->sendRequest('POST', 'contacts', $data);
    }

    /**
     * @param array $contacts Массив объектов типа Contact (у каждой контакта должен быть задан id)
     */
    public function update(array $contacts)
    {
        $data = [
            'update' => $this->getObjectsData($contacts, Contact::class)
        ];

        return $this->sendRequest('POST', 'contacts', $data);
    }

    /**
     * @param int $id ID контакта
     */
    public function get(int $id = null)
    {
        $response = $this->sendRequest(
            'GET', 'contacts', $id ? ['id' => $id]: null
        );
        
        if ($response->isSuccess())
        {
            $result = [];
            
            foreach ($response->getData()['_embedded']['items'] as $id => $item)
                $result[] = new Contact($item, $id);
        }

        return $result ? new Collections\Contacts($result) : false;
    }
}