<?php

namespace Msnet\Amocrm\Collections;

use Msnet\Amocrm\Base\Collection;

class Fields extends Collection
{
    /**
     * @return array
     */
    public function getForContacts()
    {
        return new Collection($this->data['contacts']);
    }

    /**
     * @return array
     */
    public function getForLeads()
    {
        return new Collection($this->data['leads']);
    }

    /**
     * @return array
     */
    public function getForCompanies()
    {
        return new Collection($this->data['companies']);
    }

    /**
     * @return array
     */
    public function getForCatalogs()
    {
        return new Collection($this->data['catalogs']);
    }
}