<?php

namespace Msnet\Amocrm\Collections;

use Msnet\Amocrm\Base\Collection;

class Pipelines extends Collection
{
    /**
     * @param string $status Поиск воронки, содержащей статус
     */
    public function hasStatus(string $status)
    {   
        return static::filterFunc(function($pipeline) use ($status)
        {
            foreach ($pipeline->getData()['statuses'] as $status)
            {
                if ($status->getData()->name == $status)
                    return true;
            }

            return false;
        });
    }
}