<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Setters;
use Msnet\Amocrm\Collections\PipelineStatuses;
use Msnet\Amocrm\PipelineStatus;

/**
 * https://www.amocrm.ru/developers/content/api/pipelines
 */
class Pipeline extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'name' => '',
        'sort' => 0,
        'is_main' => '',
        'statuses' => []
    ];
    
    use Setters\Id;
    use Setters\Name;
    use Setters\Sort;

    /**
     * @param array $data
     */
    public function __construct(array $data = [], int $id = null)
    {
        foreach ($data['statuses'] as &$status)
        {
            $status = new PipelineStatus($status);
        }

        $this->data['statuses'] = new PipelineStatuses($data['statuses']);
        unset($data['statuses']);

        parent::__construct($data, $id);
    }

    /**
     * @param bool $is_main Является ли главной воронкой
     */
    public function setMain(bool $is_main = true)
    {
        $this->data['is_main'] = $is_main ? 'on' : '';
    }
}