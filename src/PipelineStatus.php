<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Setters;

/**
 * https://www.amocrm.ru/developers/content/api/pipelines
 */
class PipelineStatus extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'name' => '',
        'pipeline_id' => 0,
        'sort' => '',
        'color' => '',
        'editable' => 'Y'
    ];
    
    use Setters\Id;
    use Setters\Name;
    use Setters\Sort;

    /**
     * @param bool $color Hex цвет
     */
    public function setColor(string $color)
    {
        if (strlen($color) !== 7 || $color[0] !== '#')
            throw new \Exception("Invalid hex color: $color");

        $this->data['color'] = $color;
    }

    /**
     * @param bool $is_editable Возможно ли редактирование и удаление статуса
     */
    public function setEditable(bool $is_editable = true)
    {
        $this->data['editable'] = $is_editable ? 'Y' : 'N';
    }
}