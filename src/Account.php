<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Base\Collection;
use Msnet\Amocrm\Collections;
use Msnet\Amocrm\User;

/**
 * https://www.amocrm.ru/developers/content/api/pipelines
 */
class Account extends Entity
{
    protected $data = 
    [
        'custom_fields' => [],
        'users' => [],
        'pipelines' => [],
        'groups' => [],
        'note_types' => [],
        'task_types' => []
    ];

    /**
     * @param array $data
     */
    public function __construct(array $data = [], int $id = null)
    {
        foreach ($data as $name => $value)
        {
            if ($name == 'custom_fields')     
            {
                $this->data['custom_fields'] = new Collections\Fields($value);
                continue;
            }

            if ($name == 'users')
            {
                foreach ($value as &$user)
                {
                    $user = new User($user);
                }

                $this->data[$name] = new Collections\Users($value);
                continue;
            }
            
            $this->data[$name] = new Collection($value);
        }
    }
}