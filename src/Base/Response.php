<?php

namespace Msnet\Amocrm\Base;

class Response
{
    /**
     * @var int $status
     */
    protected $status;

    /**
     * @var bool $success
     */
    protected $success = true;

    /**
     * @var array $data
     */
    protected $data;

    /**
     * @var string $error
     */
    protected $error;

    /**
     * @var array $headers
     */
    protected $headers;

    /**
     * @param int    $status
     * @param string $data
     */
    public function __construct(int $status = 200, array $data = [])
    {
        $this->status = $status;
        $this->data = $data;
        $this->success = true;
    }

    /**
     * @param string $error
     */
    public function setError(string $error)
    {
        $this->success = false;
        $this->error = $error;
        return $this;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        foreach ($headers as $name => $value)
        {
            $this->headers[$name] = $value;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}