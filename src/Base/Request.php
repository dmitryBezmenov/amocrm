<?php

namespace Msnet\Amocrm\Base;

use Msnet\Amocrm\Base\Response;

class Request
{
    /**
     * @var array $headers
     */
    protected $headers;

    /**
     * @var string $url
     */
    protected $url;

    /**
     * @var string $method
     */
    protected $method;

    /**
     * @param string $method
     * @param string $url
     * @param array  $headers
     */
    public function __construct(string $method, string $url, array $headers = [])
    {
        $this->url = $url;
        $this->method = $method;
        $this->headers = (array)$this->headers + $headers;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setHeader(string $name, string $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        foreach ($headers as $name => $value)
        {
            $this->setHeader($name, $value);
        }
    }

    /**
     * @param array|object $data
     */
    public function execute($data = null)
    {       
        $headers = [];

        foreach ($this->headers as $key => $value)
            $headers[] = "$key: $value";

        $curl = curl_init();

        curl_setopt_array($curl, 
        [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'amoCRM-API-client/1.0',
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HEADER => false,
            CURLOPT_URL => $this->method == 'GET' ? $this->url . '?' . http_build_query($data) : $this->url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_COOKIEFILE => $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt',
            CURLOPT_COOKIEJAR => $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt',
        ]);

        $text = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($code == 0)
        {
            return (
                new Response($code)
            )->setError(curl_error($curl));
        }
        else if (!in_array($code, [200, 204])) 
        {
            $response = json_decode($text, true);
            $error = '';

            if (isset($response['response'])) {
                $code = $response['response']['error_code'];
                $error = $response['response']['error'];
            } else {
                $error = $response['detail'];
            }

            return (
                new Response($code)
            )->setError((string)$error);
        } 
        else
            return new Response($code, json_decode($text, true));
    }
}