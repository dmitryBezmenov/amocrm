<?php

namespace Msnet\Amocrm\Base;

class Timer
{
    /**
     * @var int $frequency
     */
    private $frequency;

    /**
     * @var int $interval
     */
    private $interval;

    /**
     * @var array $log
     */
    private $log;

    /**
     * @param int $interval  Time in milliseconds
     * @param int $frequency Calls count
     */
    public function __construct(int $interval = 1000, int $frequency = 1)
    {
        $this->interval = $interval;
        $this->frequency = $frequency;
    }

    /**
     * @return null
     */
    public function tick()
    {
        $now = microtime(true) * 1000;

        $this->log[] = $now;
        
        $lastIntervalLog = array_values(array_filter($this->log, function($time) use ($now) {
            return $time >= $now - $this->interval; 
        }));

        if (count($lastIntervalLog) >= $this->frequency)
        {
            usleep($lastIntervalLog[0] + $this->interval);
        }
    }
}