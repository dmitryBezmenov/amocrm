<?php

namespace Msnet\Amocrm\Base;

use Msnet\Amocrm\Base\Entity;

class Collection implements \ArrayAccess, \JsonSerializable
{
    /**
     * @var array $data
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param array $filter Фильтр вида $key => значение
     */
    public function filter(array $filter)
    {
        return new static(array_values(array_filter($this->data, function($item) use ($filter)
        {
            foreach ($filter as $key => $value)
            {
                if ($item instanceof Entity)
                    $item = $item->getData();         
                else
                    $item = (object)$item;

                if (!isset($item->$key) || $item->$key !== $value)
                    return false;
            }

            return true;
        })));
    }

    /**
     * @param array $callback Функция, возвращающая true, если элемент удовлетворяет условиям
     */
    public function filterFunc(callable $callback)
    {
        return new static(array_values(array_filter($this->data, $callback)));
    }

    /**
     * Поиск первого элемента в коллекции, удовлетворяющего условию
     * 
     * @param array|callable $condition
     */
    public function find($condition)
    {
        if (is_callable($condition))
        {
            return $this->filterFunc($condition)[0];
        }
        
        if (is_array($condition))
        {
            return $this->filter($condition)[0];
        }

        throw new \Exception("Bad condition type");
    }

    /**
     * @param callable $callback   function($index, &$item) {}
     */
    public function forEach(callable $callback)
    {
        foreach ($this->data as $index => &$item)
        {
            $callback($index, $item);
        }
    }

    /**
     * @param callable $callback   function($index, &$item) {}
     * 
     * @return Collection
     */
    public function map(callable $callback)
    {
        $result = [];

        foreach ($this->data as $index => &$item)
        {
            $result[] = $callback($index, $item);
        }

        return new static($result);
    }

    /**
     * @param string $from
     * @param string $to
     * 
     * @return Collection
     */
    public function rename(string $from, string $to)
    {
        foreach ($this->data as &$item)
        {
            if ($item instanceof Entity)
            {
                $item->setField($to, $item->getField($from));
                $item->removeField($from);
            }
            elseif (is_object($item))
            {
                $item->{$to} = $item->{$from};
                unset($item->{$from});
            }
            elseif (is_array($item))
            {
                $item[$to] = $item[$from];
                unset($item[$from]);
            }
            else
            {
                throw new \Exception("Нельзя переименовать поле элементов коллекции, не являющихся объектами или массивами");
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function contains($value)
    {
        return in_array($value, $this->data);
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->data) == 0;
    }

    /**
     * @return @array
     */
    public function getValues()
    {
        return array_values($this->data);
    }

    /**
     * @param string   $name  Имя поля
     * @param callable $value Функция, возвращающая его значение function($current, $fields) {}
     * 
     * Пример:
     * 
     * $collection = new Collection([
     *     ['min' => 1],
     *     ['min' => 7, 'max' => 9],
     * ]);
     * 
     * $collection->setField('max', function($index, $current, $fields) {
     *     return $current ?: $fields['min'] + ($index + 1) * 2;
     * })
     */
    public function setField(string $name, callable $value)
    {
        $index = 0;

        foreach ($this->data as &$item)
        {
            if (is_object($item))
            {
                $item->{$name} = $value($index, $item->{$name}, $item);
            }
            else if (is_array($item))
            {
                $item[$name] = $value($index, $item[$name], $item);
            }
            else if ($item instanceof Entity)
            {
                $item->setField($name, $value($index, $item[$name], $item));
            }
            else
            {
                throw new \Exception("Нельзя установить поле для элементов коллекции, не являющихся объектами или массивами");
            }

            $index++;
        }

        return $this;
    }

    /**
     * Создать новую коллекцию с полями, перечисленными в аргументе
     * 
     * @param array $fields
     * 
     * @return Collection
     */
    public function selectFields(array $fields)
    {
        $data = $this->data;

        foreach ($data as &$item)
        {
            if ($item instanceof Entity)
                $item = $item->getData();         
            else
                $item = (object)$item;

            foreach ($item as $key => $value)
            {
                if (!in_array($key, $fields))
                {
                    unset($item->{$key});
                }
            }
        }

        return new static($data);
    }

    /**
     * Убрать поля из коллекции
     * 
     * @param array $fields
     * 
     * @return Collection
     */
    public function removeFields(array $fields)
    {
        $item = $this->data[0];

        if ($item instanceof Entity)
            $item = (array)$item->getData();         
        else
            $item = (array)$item;

        $select = [];

        foreach (array_keys($item) as $key)
        {
            if (!in_array($key, $fields))
            {
                $select[] = $key;
            }
        }

        return $this->selectFields($select);
    }

    /**
     * Array access
     */    

    public function offsetSet($index, $value) 
    {
        if (is_null($index))
            $this->data[] = $value;
        else
            $this->data[$index] = $value;
    }

    public function offsetExists($index) 
    {
        return isset($this->data[$index]);
    }

    public function offsetUnset($index) 
    {
        unset($this->data[$index]);
    }

    public function offsetGet($index) 
    {
        return isset($this->data[$index]) ? $this->data[$index] : null;
    }

    /**
     * Json serializing
     */

    public function jsonSerialize()
    {
        return $this->data;
    }
}