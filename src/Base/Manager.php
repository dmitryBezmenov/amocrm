<?php

namespace Msnet\Amocrm\Base;

use Msnet\Amocrm\Base\Client;
use Msnet\Amocrm\Base\Collection;

class Manager
{
    /**
     * @var Client $client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $action
     * @param string $method
     * @param $data
     */
    protected function sendRequest(string $method, string $action, $data = null, bool $base = false)
    {
        $request = $base
            ? $this->client->createBaseRequest($method, $action)
            : $this->client->createRequest($method, $action);

        $response = $request->execute($data ?: $this->data);

        return $response;
    }

    /**
     * @param array  $items
     * @param string $class
     * 
     * @return Collection
     */
    protected function getObjectsData(array $items, string $class)
    {
        $result = [];

        foreach ($items as $item)
        {
            if (!($item instanceof $class))
                throw new \Exception("Invalid object type");

            $result[] = $item->getData();
        }

        return new Collection($result);
    }

}