<?php

namespace Msnet\Amocrm\Setters;

use Msnet\Amocrm\Field;
use Msnet\Amocrm\Subtypes\Value;

trait CustomFields
{
    /**
     * @param string $name   Кастомное поле
     * @param array  $values Массив значений типа Value
     */
    public function addCustomField(Field $field, $values)
    {
        $field = $field->getData();

        foreach ($values as &$value)
        {
            if (!($value instanceof Value))
                throw new \Exception("Bad value type $value");

            $value = $value->getData();

            if (isset($value['enum']) && !empty($field->enums) && !in_array($value['enum'], $field->enums))
                throw new \Exception("Value not in available values of enumerate field: {$field->name}:$value");
        }
        
        $this->data['custom_fields'][] = 
        [
            'id' => $field->id,
            'values' => $values
        ];      

        return $this;
    }
}