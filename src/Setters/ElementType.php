<?php

namespace Msnet\Amocrm\Setters;

trait ElementType
{
    /**
     * @param int $element_type Тип элемента
     */
    public function setElementType(int $element_type)
    {
        $this->data['element_type'] = $element_type;
    }
}