<?php

namespace Msnet\Amocrm\Setters;

trait Name
{
    /**
     * @param string $name Имя сделки
     */
    public function setName(string $name)
    {
        $this->data['name'] = $name;      

        return $this;
    }
}