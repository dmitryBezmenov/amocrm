<?php

namespace Msnet\Amocrm\Setters;

use Msnet\Amocrm\Field;

trait UpdatedAt
{
    /**
     * @param string $date Unix время обновления
     */
    public function setUpdatedAt(string $date)
    {
        if (strlen($date) !== 10)
            throw new \Exception("Invalid date: $date");

        $this->data['updated_at'] = $date;      

        return $this;
    }
}