<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Setters;

class User extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'name' => '',
        'last_name' => '',
        'login' => '',
        'language' => '',
        'group_id' => '',
        'is_active' => false,
        'is_free' => false,
        'is_admin' => false,
        'phone_number' => '',
        'rights' => [],
    ];
    
    use Setters\Id;
    use Setters\Name;
}