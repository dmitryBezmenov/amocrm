<?php

namespace Msnet\Amocrm\Enums;

class FieldEmailType
{
    const WORK = 'WORK';
    const PRIV = 'PRIV';
    const OTHER = 'OTHER';
}