<?php

namespace Msnet\Amocrm\Enums;

class NoteType
{
    const COMMON = 4;
    const EXTERNAL_ATTACHMENT = 9;
    const TASK_RESULT = 13;
    const SYSTEM = 25;
    const SMS_IN = 102;
    const SMS_OUT = 103;
}