<?php

namespace Msnet\Amocrm\Enums;

class FieldConnectType
{
    const SKYPE = 'SKYPE';
    const ICQ = 'ICQ';
    const JABBER = 'JABBER';
    const GTALK = 'GTALK';
    const MSN = 'MSN';
    const OTHER = 'OTHER';
}