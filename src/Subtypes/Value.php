<?php

namespace Msnet\Amocrm\Subtypes;

class Value
{
    /**
     * @var $value
     */
    private $value;

    /**
     * @var string $enum
     */
    private $enum;

    /**
     * @var string $subtype
     */
    private $subtype;

    /**
     * @param        $value   Само значение
     * @param string $enum    Код варианта поля перечислимого типа
     * @paran string $subtype Подтип (для адреса)
     */
    public function __construct($value, string $enum = '', string $subtype = '')
    {
        $this->value = $value;
        $this->enum = $enum;
        $this->subtype = $subtype;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [
            'value' => $this->value
        ];

        if (!empty($this->enum))
            $data['enum'] = $this->enum;

        if (!empty($this->subtype))
            $data['subtype'] = $this->subtype;

        return $data;
    }
}