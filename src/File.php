<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Setters;

class File extends Entity
{
    protected $data = 
    [
        'path' => '',
        'name' => '',
        'element_id' => 0,
        'element_type' => 0,
    ];
    
    use Setters\ElementId;
    use Setters\ElementType;
    
    /**
     * @param string $path Полный путь к файлу
     */
    public function setPath(string $path)
    {
        $this->data['path'] = $path;

        if (!$this->data['name'])
        {
            $this->data['name'] = basename($path);
        }
    }

    /**
     * @param string $name Имя файла
     */
    public function setName(string $name)
    {
        $this->data['name'] = $name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->data['path'];
    }
}