<?php

namespace Msnet\Amocrm;

use Msnet\Amocrm\Base\Entity;
use Msnet\Amocrm\Base\Collection;
use Msnet\Amocrm\Setters;

/**
 * https://www.amocrm.ru/developers/content/api/fields
 */
class Field extends Entity
{
    protected $data = 
    [
        'id' => 0,
        'name' => '',
        'field_type' => 0,
        'sort' => 0,
        'is_multiple' => false,
        'is_system' => false,
        'is_editable' => false,
        'is_required' => false,
        'is_visible' => false,
        'params' => [],
        'enums' => [],
    ];
    
    use Setters\Id;
    use Setters\Name;
    use Setters\Sort;

    /**
     * @return stdObject
     */
    public function getData()
    {
        $data = (object)$this->data;

        $enums = [];
        foreach ($data->enums as $id => $enum)
        {
            $enums[] = (object)[
                'id' =>  $id,
                'value' =>  $enum
            ];
        }
        $data->enums = new Collection($enums);
        
        $data->type = $data->field_type;
        
        return $data;
    }
    
    /**
     * @param string $code Код
     */
    public function setMain(bool $is_main = true)
    {
        $this->data['is_main'] = $is_main ? 'on' : '';
    }

    /**
     * @param bool $is_multiple Является ли множественным
     */
    public function setMultiple($is_multiple = true)
    {
        $this->data['is_multiple'] = $is_multiple;
    }

    /**
     * @param bool $is_system Является ли системным
     */
    public function setSystem($is_system = true)
    {
        $this->data['is_system'] = $is_system;
    }

    /**
     * @param bool $is_editable Является ли редактируемым
     */
    public function setEditable($is_editable = true)
    {
        $this->data['is_editable'] = $is_editable;
    }

    /**
     * @param bool $is_required Является ли требуемым
     */
    public function setRequired($is_required = true)
    {
        $this->data['is_required'] = $is_required;
    }

    /**
     * @param bool $is_visible Является ли видимым
     */
    public function setVisible($is_visible = true)
    {
        $this->data['is_visible'] = $is_visible;
    }

    /**
     * @param string $variant Вариант значения
     */
    public function addEnum(string $variant)
    {
        $this->data['enums'][] = $variant;
    }
}